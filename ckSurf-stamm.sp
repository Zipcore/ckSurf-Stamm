#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <multicolors>
#include <cstrike>
#include <stamm>
#include <cksurf>

#define PL_VERSION "1.0"

public Plugin myinfo = 
{
	name        = "ckSurf Stamm",
	author		= ".#Zipcore",
	description = "",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

public Action ckSurf_OnMapFinished(int client, float fRunTime, char sRunTime[54], int rank, int total)
{
	GivePoints(client, fRunTime);
}

public Action cckSurf_OnBonusFinished(int client, float fRunTime, char sRunTime[54], int rank, int total, int bonusid)
{
	GivePoints(client, fRunTime);
}

void GivePoints(int client, float time)
{
	int iPoints = RoundToFloor(time / 30.0);
	
	if(iPoints < 1)
		iPoints = 1;
	
	if(STAMM_AddClientPoints(client, iPoints))
		CPrintToChat(client, "You got {darkred}%d Klikpoint%s {lightgreen}for finishing this map.", iPoints, iPoints > 1 ? "s" : "");
}